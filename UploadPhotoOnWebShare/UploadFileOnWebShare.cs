﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Timers;

namespace UploadPhotoOnWebShare
{
    public partial class UploadFileOnWebShare : ServiceBase
    {
        Timer timer = new Timer();
        public UploadFileOnWebShare()
        {
            InitializeComponent();
            if (!System.Diagnostics.EventLog.SourceExists("UploadOnWebShare"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "UploadOnWebShare", "UploadFileLog");
            }
            eventLog1.Source = "UploadOnWebShare";
            eventLog1.Log = "UploadFileLog";
        }

        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("Started File Upload");
            //var uploadOnAmazon = new UploadFileOnAmazon();
            //uploadOnAmazon.StartUpload();
           
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            //This statement is used to set interval to 5 minute (= 60,000 milliseconds)

            timer.Interval = 300000;

            //enabling the timer
            timer.Enabled = true;
           
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            eventLog1.WriteEntry("Elaspsed Time " + DateTime.Now);
        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("Ended File Upload");
            timer.Enabled = false;
        }
    }
}
