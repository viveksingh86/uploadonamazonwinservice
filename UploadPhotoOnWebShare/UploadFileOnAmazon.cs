﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using System.IO;
using System.Drawing;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Specialized;

namespace UploadPhotoOnWebShare
{
    public class UploadFileOnAmazon
    {
        private SqlConnection connection;
        private DataSet ds;
        private SqlCommand command;
        private SqlDataAdapter dataAdapter; 

        public void StartUpload()
        {
            // Save the new graphic file to the server
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["PhotoContext"].ConnectionString;
                connection = new System.Data.SqlClient.SqlConnection(connectionString);
                connection.Open();
                ds = new System.Data.DataSet();
                command = new System.Data.SqlClient.SqlCommand();
                command.Connection = connection;
                command.CommandText = "Select * from [Photos] where IsDeleted = 0 and IsUploaded = 0";
                dataAdapter = new System.Data.SqlClient.SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                SqlCommandBuilder cb = new SqlCommandBuilder(dataAdapter);
                // dataAdapter.TableMappings.Add("Table", "Photos");
                dataAdapter.Fill(ds);
                           

                foreach (DataRow drRow in ds.Tables[0].Rows)
                {
                    //filename = drRow["EntityGuid"].ToString().ToLower() + "_" + drRow["FileName"].ToString() + "_400.png";
                    //filepath = "C:\\Users\\Vivek\\Documents\\visual studio 2012\\Projects\\UploadPhoto\\UploadPhoto\\Images\\resized\\" + drRow["FileName"].ToString() + "_400.png";
                    //upload thumbnail
                    GetConfigAndUpload("ThumbNail", drRow["EntityGuid"].ToString().ToLower(), drRow["FileName"].ToString());
                    GetConfigAndUpload("Resized", drRow["EntityGuid"].ToString().ToLower(), drRow["FileName"].ToString());
                    if ((bool)drRow["IsUploaded"] == false)
                    {
                        drRow["IsUploaded"] = true;
                    }
                    
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dataAdapter.Update(ds.Tables[0]);
                    ds.AcceptChanges();
                }
                connection.Close();
                command.Dispose();
                
            }
            catch (Exception)
            {
                connection.Close();
                command.Dispose();
                throw;
            }
          
        }


        public void GetConfigAndUpload(string settingName, string entityGUID, string fileName)
        {
            // Save the new graphic file to the server
            string filepath, filesize, sectionName;
            //sectionName = "ImagePath/" + settingName;
            sectionName = settingName;
            var section = ConfigurationManager.GetSection(sectionName) as NameValueCollection;
            filepath = section["path"];
            filesize = section["size"];

            var name = entityGUID + "_" + fileName + "_" + filesize + ".png";
            var path = filepath + fileName + "_" + filesize + ".png";
            UploadFileOnWeb(path, name);
           

        }

        public void UploadFileOnWeb(string filepath, string filename)
        {
            // Save the new graphic file to the server
            IAmazonS3 client;


            using (client = AWSClientFactory.CreateAmazonS3Client())
            {

                PutObjectRequest request = new PutObjectRequest();
                request.BucketName = "contactvivek086";
                request.CannedACL = S3CannedACL.PublicRead;
                request.Key = "images/" + filename;
                request.FilePath = filepath;
                PutObjectResponse response = client.PutObject(request);
            }

        }

    }
}
