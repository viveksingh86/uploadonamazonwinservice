﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UploadPhotoOnWebShare
{
    public class FileConfig : ConfigurationSection
    {
        public String Path
        {
            get { return this["key"].ToString(); }
            set { this["key"] = value; }
        }

        public String Size
        {

            get { return this["key"].ToString(); }
            set { this["key"] = value; }
        }
    }
}
